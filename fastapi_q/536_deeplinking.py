import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.responses import Response
from starlette.responses import HTMLResponse

app = FastAPI(
    docs_url="/",
    redoc_url=None,
    swagger_ui_oauth2_redirect_url=None,
    openapi_url="/openapi.json",
)


class M(BaseModel):
    h1: str
    h2: str
    h3: str
    h4: str
    h5: str
    h6: str
    h7: str
    h8: str


@app.get("/1", response_model=M)
async def i1():
    return Response()


@app.get("/2", response_model=M)
async def i2():
    return Response()


@app.get("/3", response_model=M)
async def i3():
    return Response()


@app.get("/4", response_model=M)
async def i4():
    return Response()


if __name__ == "__main__":
    uvicorn.run(app=app)
