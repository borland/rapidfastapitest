import httpx
import pytest
import uvicorn
from async_asgi_testclient import TestClient
from fastapi import FastAPI, Header


app = FastAPI()

# authorization in swagger you get 422 and not in tests
@app.get("/t")
async def get_current_user_info(authorization: str = Header(...)):
    return authorization


@pytest.mark.asyncio
async def test_header_with_authorization_as_name():
    headers = {"authorization": "anystring"}
    async with TestClient(app) as client:
        response = await client.get("/t", headers=headers)
        assert response.status_code == 200


    async with httpx.AsyncClient(app=app, base_url="http://domain.tld") as client:
        response = await client.get("/t", headers=headers)
        assert response.status_code == 200


if __name__ == '__main__':
    uvicorn.run("612_pass_header_in_swagger:app", reload=True)
