from typing import Optional

from pydantic import BaseModel

import base64


class File(BaseModel):
    name: str
    content: bytes
    type: Optional[str]

    class Config:
        orm_mode = True
        json_encoders = {bytes: lambda v: base64.b64encode(v).decode()}


# with open("example.xlsx", "rb") as excelf:
#     content = excelf.read()

content = b"\x01" * 3
f = File(name="myname", content=content)
print(base64.b64encode(content))
print(base64.b64encode(content).decode())
print(f)
print(f.json())
