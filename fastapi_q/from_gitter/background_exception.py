import logging
import logging as log
from time import time
import random

import pytest
from async_asgi_testclient import TestClient
from fastapi import HTTPException
from fastapi import BackgroundTasks, FastAPI

app = FastAPI()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


async def backgrounded_task(text):
    logger.error(text)

@app.get("/yolo")
async def yolo(trigger: int, background_tasks: BackgroundTasks):
    if trigger == 0:
        background_tasks.add_task(backgrounded_task, "buuuuuuuuuuu")
        raise HTTPException(status_code=400, detail="X-Token header invalid")
    else:
        background_tasks.add_task(backgrounded_task, "yeeeeeeeee")
        return {"Success": True}


@pytest.mark.asyncio
async def test_background(caplog):
    async with TestClient(app) as client:
        resp = await client.get("/yolo?trigger=1")
        assert resp.status_code == 200
        assert "yeeeeeeeee" in caplog.messages
    async with TestClient(app) as client:
        resp = await client.get("/yolo?trigger=0")
        assert resp.status_code == 400
        assert "buuuuuuuuuuu" in caplog.messages

