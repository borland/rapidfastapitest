import uvicorn
from fastapi import FastAPI, APIRouter

app = FastAPI()


@app.on_event("startup")
async def startup():
    app.state.toto = "toto"

router = APIRouter()
router.state = app.state


@router.get("/")
async def router1():

    return {"OK": router.state}

app.include_router(router)

if __name__ == '__main__':
    uvicorn.run("state_in_router:app", reload=True)