import uvicorn
from fastapi import FastAPI

app = FastAPI(redoc_url="/docs", docs_url=None)

if __name__ == '__main__':
    uvicorn.run(app)