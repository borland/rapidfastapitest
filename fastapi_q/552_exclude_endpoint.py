import os
from functools import lru_cache
from typing import Optional

import uvicorn
from fastapi import FastAPI
from pydantic import BaseSettings


class AppSettings(BaseSettings):
    excluded_endpoint_def_list: Optional[str]

    class Config:
        env_prefix = ""


@lru_cache()
def get_app_settings() -> AppSettings:
    return AppSettings()


def get_app() -> FastAPI:
    app_settings = get_app_settings()

    app = FastAPI()

    @app.get("/included")
    async def included_list():
        return {"included": True}

    @app.get("/excluded")
    async def excluded_list():
        return {"excluded": True}

    if app_settings.excluded_endpoint_def_list is not None:
        excluded_endpoint_list = app_settings.excluded_endpoint_def_list.split(",")
        routes_to_exclude = [r for r in app.routes if r.name in excluded_endpoint_list]
        for route_to_exclude in routes_to_exclude:
            app.routes.remove(route_to_exclude)

    return app


if __name__ == "__main__":
    os.environ["EXCLUDED_ENDPOINT_DEF_LIST"] = "excluded_list"
    uvicorn.run(get_app())
