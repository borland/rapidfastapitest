import json

import fastapi
from fastapi import FastAPI, Depends, Query
from fastapi.openapi.models import OAuthFlows, OAuthFlowAuthorizationCode, \
    OAuthFlowClientCredentials
from fastapi.security import OAuth2PasswordBearer
from starlette.config import Config
from starlette.middleware.sessions import SessionMiddleware
from starlette.requests import Request
from starlette.responses import HTMLResponse, RedirectResponse
from authlib.integrations.starlette_client import OAuth

init_oauth = {
    "clientId": "454579872863-6hs49f5s56f5olqbi8qnf7n7t0u6tjd8.apps.googleusercontent.com",
    "appName": "euri10fastapibase",
    "clientSecret": "Dy8cFebcs0JaR151kL7oaNjq"
  }

app = FastAPI(debug=True)
# app = FastAPI(debug=True, swagger_ui_init_oauth=init_oauth)
# app = FastAPI(debug=True)
app.add_middleware(SessionMiddleware, secret_key="!secret")


config = Config('.oauth2google.env')
oauth = OAuth(config)

CONF_URL = 'https://accounts.google.com/.well-known/openid-configuration'
oauth.register(
    name='google',
    server_metadata_url=CONF_URL,
    client_kwargs={
        'scope': 'openid email profile'
    }
)
from fastapi.security.oauth2 import OAuth2
oauth2_scheme = OAuth2(
    flows=OAuthFlows(
        authorizationCode=OAuthFlowAuthorizationCode(scopes={"email": "email info"}, authorizationUrl="https://accounts.google.com/o/oauth2/v2/auth", tokenUrl="https://oauth2.googleapis.com/token")
    )
)


@app.route('/')
async def homepage(request):
    user = request.session.get('user')
    if user:
        data = json.dumps(user)
        html = (
            f'<pre>{data}</pre>'
            '<a href="/logout">logout</a>'
        )
        return HTMLResponse(html)
    return HTMLResponse('<a href="/login">login</a>')


@app.route('/login')
async def login(request):
    redirect_uri = request.url_for('auth')
    return await oauth.google.authorize_redirect(request, redirect_uri)


@app.route('/auth')
async def auth(request):
    token = await oauth.google.authorize_access_token(request)
    user = await oauth.google.parse_id_token(request, token)
    request.session['user'] = dict(user)
    return RedirectResponse(url='/')


@app.route('/logout')
async def logout(request):
    request.session.pop('user', None)
    return RedirectResponse(url='/')


@app.get("/test")
async def test_list(token: str = Depends(oauth2_scheme)):
    return "ok"


if __name__ == '__main__':
    import uvicorn
    uvicorn.run("oauth2:app", host='127.0.0.1', port=8000, reload=True)