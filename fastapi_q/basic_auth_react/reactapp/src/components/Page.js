import React from 'react';
import {useGoogleLogin} from "react-use-googlelogin";

const Page = (props) => {
    const {isSignedIn, isInitalized, signIn, signOut} = useGoogleLogin({clientId: "454579872863-6hs49f5s56f5olqbi8qnf7n7t0u6tjd8.apps.googleusercontent.com"});

    return (
        <div>
            <h2>Content</h2>
                <>
                    {isSignedIn ? (
                        <button onClick={signOut}>Sign Out</button>
                    ) : (
                        <button onClick={signIn}>Sign In</button>
                    )}
                </>
        </div>
    )
};


export default Page;
