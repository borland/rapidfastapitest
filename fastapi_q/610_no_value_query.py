import logging

import pytest
from async_asgi_testclient import TestClient
from fastapi import FastAPI, Query
from starlette.requests import Request

app = FastAPI()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@app.get("/items")
async def read_items(
    request: Request, q: str = Query(None), my_flag: bool = Query(None)
):
    qp = request.query_params
    logger.debug(qp)
    return {"q": q, "my_flag": my_flag}


@pytest.mark.asyncio
async def test_empty():
    client = TestClient(app)
    response = await client.get("/items?my_flag&q=1")
    assert response.status_code == 200
    assert response.json() == {"q": "1", "my_flag": None}

    # response = await client.get("/items?q=1")
    # assert response.status_code == 200
    # assert response.json() == {"q": "1", "my_flag": None}
