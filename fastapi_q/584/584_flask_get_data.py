import pickle

import magic
import pytest
from async_asgi_testclient import TestClient
from fastapi import FastAPI
from starlette.requests import Request


# r = requests.post(url,data=data)

app = FastAPI()


@app.post("/image")
async def image_create(request: Request):
    body = await request.body()
    l = pickle.loads(body)
    mime = magic.from_buffer(l)
    return {"mime": mime}


img_data = [("raw.gif", "GIF image data"), ("raw.jpeg", "JPEG image data")]


@pytest.mark.parametrize("img, expected_mime", img_data)
@pytest.mark.asyncio
async def test_mime(img, expected_mime):
    with open(img, "rb") as im:
        r = im.read()
        p = pickle.dumps(r)
    async with TestClient(app) as client:
        response = await client.post("/image", data=p)
        assert response.status_code == 200
        assert expected_mime in response.json().get("mime")
