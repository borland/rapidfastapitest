from pathlib import PosixPath

from pydantic import BaseModel, Path
from starlette.testclient import TestClient

from fastapi import FastAPI


class ContainsPathProperty(BaseModel):
    path: Path

    class Config:
        json_encoders = {PosixPath: lambda v: v.as_posix()}


app = FastAPI()


@app.get("/", response_model=ContainsPathProperty)
async def get_main():
    obj = ContainsPathProperty(path=Path("/some/random/path"))
    return obj


client = TestClient(app)


def test_serialize_path():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"path": "/some/random/path"}
