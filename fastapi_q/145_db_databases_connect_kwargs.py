import asyncio

from databases import Database, DatabaseURL


async def main():
    dburl = DatabaseURL("postgresql://")
    database = Database(
        dburl, host="localhost", port=5438, user="postgres", password="postgres"
    )

    await database.connect()

    query = """SELECT * FROM pg_catalog.pg_tables"""
    r = await database.execute(query=query)
    print(r)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
