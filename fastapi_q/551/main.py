import uuid
from random import getrandbits

import sqlalchemy
from fastapi import FastAPI
from databases import Database
from sqlalchemy import select, and_


app = FastAPI()
dburl = "postgresql://postgres:postgres@localhost:5438/postgres"
db = Database(dburl)


metadata = sqlalchemy.MetaData()
notes = sqlalchemy.Table(
    "notes",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("text", sqlalchemy.String(length=100)),
    sqlalchemy.Column("completed", sqlalchemy.Boolean),
)

engine = sqlalchemy.create_engine(dburl)
metadata.create_all(engine)


@app.on_event("startup")
async def startup():
    await db.connect()


@app.on_event("shutdown")
async def shutdown():
    await db.disconnect()


@app.post("/note")
async def note_create():
    query = notes.insert()
    values = [
        {"text": str(uuid.uuid4()), "completed": bool(getrandbits(1))}
        for i in range(100000)
    ]
    await db.execute_many(query, values)
    return "ok"


@app.get("/note")
async def note_list():
    query = (
        select([notes.c.text])
        .where(and_(notes.c.text.like("%24%"), notes.c.completed == True))
        .order_by(notes.c.id)
        .limit(50)
    )
    result = await db.fetch_all(query)
    return result


# if __name__ == "__main__":
#     pass
#     # uvicorn 551_app:app --host=0.0.0.0 --port=8000
#     # Requests per second:    221.70 [#/sec] (mean)
#     # gunicorn -w=4 -k uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000 551_app:app
#     # Requests per second:    1073.11 [#/sec] (mean)
#
#     # gunicorn -w=4 --threads=2 -k uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000 551_app:app
#     # Requests per second:    1230.40 [#/sec] (mean)
#     # gunicorn -w=4 --threads=4 -k uvicorn.workers.UvicornWorker --bind 0.0.0.0:8000 551_app:app
#     # Requests per second:    1222.16 [#/sec] (mean)
