from enum import Enum

from pydantic import BaseSettings


class WorkerEnum(str, Enum):
    UvicornWorker = "uvicorn.workers.UvicornWorker"
    UvicornH11Worker = "uvicorn.workers.UvicornH11Worker"


class GunicornSettings(BaseSettings):

    worker_class: str = WorkerEnum.UvicornWorker
    workers: int = 2
    threads: int = 1
    bind: str = "0.0.0.0:8000"
    backlog: int = 2048

    class Config:
        env_prefix = "GUNICORN_"
