import uvicorn
from fastapi import FastAPI

# Create new application instance
app = FastAPI()


@app.middleware("http")
async def add_custom_header(request, call_next):
    response = await call_next(request)
    response.headers['content-type'] = 'application/geo+json'
    return response


@app.get('/content-type-test')
async def content_type_test():
    return {}


if __name__ == '__main__':
    # Run the application locally
    uvicorn.run(app, host='127.0.0.1', port=8000)