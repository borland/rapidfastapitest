from typing import Any

from fastapi import FastAPI, Body, Form
from starlette.requests import Request
from starlette.responses import Response
from starlette.testclient import TestClient

app = FastAPI()


@app.post("/webhook")
async def the_webhook(request: Request, body: Any = Form(...)):

    # return await request.body()
    return body


data = b"""EURUSD Less Than 1.09092
{"Condition": "value"}
[3,4,5,]
{}"""

client = TestClient(app)
response = client.post("/webhook", data={"data": data})
print(response.content)
print(response.headers)
# b'EURUSD Less Than 1.09092\n{"Condition": "value"}\n[3,4,5,]\n{}'
