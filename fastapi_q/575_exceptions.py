import pytest
from async_asgi_testclient import TestClient
from fastapi import FastAPI
from starlette.requests import Request

app = FastAPI()


@app.exception_handler(Exception)
async def exception_callback(request: Request, exc: Exception):
    print(request.query_params)
    print(request.headers)
    print(request.path_params)
    print(exc.args)


@app.post("/create_workflow")
async def create_workflow():
    raise Exception(13)


@pytest.mark.asyncio
async def test_raise_exc():
    async with TestClient(app) as client:
        with pytest.raises(Exception) as pe:
            response = await client.post("/create_workflow")
