import pytest
from fastapi import FastAPI, Header
from starlette.testclient import TestClient

app = FastAPI()


@app.get("/")
async def root(foo_bar: str = Header(..., min_length=10, convert_underscores=False)):
    return {"message": foo_bar}


def test_header_lenght():
    with TestClient(app) as client:
        response = client.get("/", headers={"foo_bar": "0123456789"})
        assert response.status_code ==200

    with TestClient(app) as client:
        response = client.get("/", headers={"foo_bar": "012345678"})
        assert response.status_code ==422