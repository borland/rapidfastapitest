import asyncio
import logging

from fastapi import FastAPI

import uvicorn

app = FastAPI()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

if not logger.hasHandlers():
    sh = logging.StreamHandler()
    fmt = logging.Formatter(fmt="%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
    sh.setFormatter(fmt)
    logger.addHandler(sh)


class Service(object):
    def __init__(self):
        self.counter = 0

    def startup(self):
        logger.debug("service startup")
        self.task = asyncio.create_task(self.tick())

    def shutdown(self):
        self.task.cancel()
        logger.debug("service shutdow")

    async def tick(self) -> None:
        while True:
            self.counter += 1
            await asyncio.sleep(1)
            logger.debug(f"counter is at: {self.counter}")


@app.on_event("startup")
async def startup():
    service = Service()
    service.startup()
    app.state.service = service
    logger.debug('end startup lifespan')


@app.on_event("shutdown")
async def shutdown():
    service = app.state.service
    service.shutdown()
    logger.debug('end shutdown lifespan')


if __name__ == '__main__':
    uvicorn.run("617_events:app", reload=True)
