import uvicorn
from fastapi import FastAPI
import asyncio

import logging

logger: logging.Logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

if not logger.hasHandlers():
    sh = logging.StreamHandler()
    fmt = logging.Formatter(fmt="%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
    sh.setFormatter(fmt)
    logger.addHandler(sh)
    logger.propagate = False

app = FastAPI()


@app.get("/")
async def read_root():
    logger.debug('Started')
    await asyncio.sleep(5)
    logger.debug('Finished')
    return {"Hello": "World"}


if __name__ == '__main__':
    uvicorn.run("625_asyn_question:app")