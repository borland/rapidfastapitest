import logging
import sys
from collections import OrderedDict
from typing import List

import asyncpg
import uvicorn
from asyncpg import (
    PostgresConnectionError,
    ConnectionDoesNotExistError,
    SyntaxOrAccessError,
    Connection,
)
from fastapi import FastAPI, Depends
from pydantic import BaseModel
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR, HTTP_201_CREATED

from settings import get_db_settings

app = FastAPI(debug=True)
database_pool = None

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@app.on_event("startup")
async def startup():
    global database_pool
    dbs = get_db_settings()
    database_pool = await asyncpg.create_pool(
        host=dbs.host,
        port=dbs.port,
        user=dbs.user,
        password=dbs.password,
        database=dbs.db,
        min_size=dbs.min_size,
        max_size=dbs.max_size,
        max_inactive_connection_lifetime=dbs.max_inactive_connection_lifetime,
    )


@app.on_event("shutdown")
async def shutdown():
    await database_pool.close()


@app.middleware("http")
async def middleware(request: Request, call_next):
    try:
        return await call_next(request)
    finally:
        if hasattr(request.state, "db"):
            active_connections = await request.state.db.execute(
                "select count(*) from pg_stat_activity where state='active'"
            )
            logger.debug(active_connections)
            await database_pool.release(request.state.db)


class Note(BaseModel):
    text: str
    completed: bool


async def get_db(request: Request) -> Connection:
    try:
        conn: Connection = await database_pool.acquire()
        try:
            await conn.fetch("SELECT 1")
        except ConnectionDoesNotExistError:
            conn = await database_pool.acquire()
        request.state.db = conn
        return conn
    except (PostgresConnectionError, OSError) as e:
        logger.error("Unable to connect to the database: %s", e)
        raise HTTPException(
            detail="Unable to connect to the database.",
            status_code=HTTP_500_INTERNAL_SERVER_ERROR,
        )
    except SyntaxOrAccessError as e:
        logger.error("Unable to execute query: %s", e)
        raise HTTPException(
            detail="Unable to execute the required query to obtain data from the database.",
            status_code=HTTP_500_INTERNAL_SERVER_ERROR,
        )


@app.post("/simpleselect")
async def simpleselect_create(db: asyncpg.Connection = Depends(get_db)):
    await db.fetch("SELECT 1")
    return {"OK"}


@app.post("/note", status_code=HTTP_201_CREATED)
async def note_create(note: Note, db: asyncpg.Connection = Depends(get_db)):
    query = "INSERT INTO notes (text, completed) VALUES ($1, $2) RETURNING (note_id, text, completed)"
    r = await db.fetch(query, note.text, note.completed)
    return {"created": r}


@app.post("/notes", status_code=HTTP_201_CREATED)
async def notes_create(notes: List[Note], db: asyncpg.Connection = Depends(get_db)):
    query = "INSERT INTO notes (text, completed) VALUES ($1, $2) RETURNING (note_id, text, completed)"
    r = await db.executemany(query, [(note.text, note.completed) for note in notes])
    return {"created": len(notes)}


@app.get("/notes")
async def notes_list(db: asyncpg.Connection = Depends(get_db)):
    query = "SELECT text, completed FROM notes"
    all_notes = await db.fetch(query)
    return all_notes


class Record(OrderedDict):
    """
    by @fgimian
    Provides a very similar Record class to that returned by asyncpg.  A custom implementation
    is needed as it is currently impossible to create asyncpg Record objects from Python code.
    """

    def __getitem__(self, key_or_index):
        if isinstance(key_or_index, int):
            return list(self.values())[key_or_index]

        return super().__getitem__(key_or_index)

    def __repr__(self):
        return "<{class_name} {items}>".format(
            class_name=self.__class__.__name__,
            items=" ".join(f"{k}={v!r}" for k, v in self.items()),
        )


if __name__ == "__main__":
    print(sys.path)
    uvicorn.run("app:app", reload=True)
