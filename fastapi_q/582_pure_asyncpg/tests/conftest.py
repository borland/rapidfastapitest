import asyncio
import logging
import os
from pathlib import Path

import asyncpg
import pytest
from asyncpg import Connection
from asynctest import Mock

from app import app, get_db
from settings import get_db_settings, DBSettings

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

DBTEST = "postgres_test"


async def drop_database(con: Connection, db: str):
    query = "drop database postgres_test;"
    r = await con.execute(query)
    return r


@pytest.yield_fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(autouse=True, scope="session")
async def create_test_database():
    dbs: DBSettings = get_db_settings(Path("../tests.env"))
    if os.environ.get("RUN_IN_DOCKER"):
        con: Connection = await asyncpg.connect(
            host=dbs.host, port=dbs.port, user=dbs.user, password=dbs.password
        )
    else:
        con: Connection = await asyncpg.connect(
            host=dbs.host_docker, port=dbs.port, user=dbs.user, password=dbs.password
        )
    query = (
        "select exists(select datname from pg_catalog.pg_database where datname=$1);"
    )
    dbtest_exists = await con.fetchrow(query, dbs.db)
    assert dbs.db == DBTEST
    logger.debug(dbtest_exists)
    if dbtest_exists.get("exists"):
        await drop_database(con, "postgres_test")
    query = "create database postgres_test;"
    await con.execute(query)
    # p = os.path.join(os.getcwd(), "alembic.ini")
    # m = os.path.join(os.getcwd(), "migrations")
    # alembic_config = Config(p)  # Run the migrations.
    # alembic_config.set_main_option("script_location", m)
    # alembic_config.attributes["configure_logger"] = False
    # logger.debug("About to run alembic upgrade in tests")
    # command.upgrade(alembic_config, "head")
    yield  # Run the tests.
    await drop_database(con, "postgres_test")
    logger.debug("Drop db end of session fixture")


@pytest.fixture(scope="function")
def mocked_db(request):
    def fin():
        del app.dependency_overrides[get_db]

    db = Mock()
    app.dependency_overrides[get_db] = lambda: db
    request.addfinalizer(fin)
    return db
