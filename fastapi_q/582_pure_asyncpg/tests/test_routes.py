import pytest
from async_asgi_testclient import TestClient as AsyncTestClient
from asynctest import CoroutineMock, call

from app import app, Record


@pytest.mark.asyncio
async def test_simpleselect_create(mocked_db):
    async with AsyncTestClient(app) as client:
        mocked_db.fetch = CoroutineMock(return_value=[])
        response = await client.post("/simpleselect")
        assert response.status_code == 200
        assert response.json() == ["OK"]
        mocked_db.fetch.assert_awaited_once()
        assert mocked_db.fetch.await_args == call("SELECT 1")


@pytest.mark.asyncio
async def test_note_create(mocked_db):
    async with AsyncTestClient(app) as client:
        note = {"text": "test", "completed": False}
        record = Record(note)
        mocked_db.fetch = CoroutineMock(return_value=record)
        response = await client.post(app.url_path_for("note_create"), json=note)
        assert response.status_code == 201
        assert response.json() == {"created": note}
        mocked_db.fetch.assert_awaited_once()
        assert mocked_db.fetch.await_args == call(
            "INSERT INTO notes (text, completed) VALUES ($1, $2) RETURNING (note_id, text, completed)",
            "test",
            False,
        )


@pytest.mark.asyncio
async def test_notes_list(mocked_db):
    async with AsyncTestClient(app) as client:
        notes = [{"text": f"text{1}", "completed": True} for i in range(5)]
        records = [Record(note) for note in notes]
        mocked_db.executemany = CoroutineMock(return_value=None)
        response = await client.post(app.url_path_for("notes_create"), json=notes)
        assert response.status_code == 201
        assert response.json() == {"created": len(notes)}
        mocked_db.fetch = CoroutineMock(return_value=records)
        response = await client.get(app.url_path_for("notes_list"))
        assert response.status_code == 200
        assert response.json() == notes
