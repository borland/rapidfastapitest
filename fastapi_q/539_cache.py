import asyncio
import logging
import random
from typing import Dict

import aiocache
import pytest
import uvicorn

from aiocache import cached, SimpleMemoryCache
from async_asgi_testclient import TestClient

from fastapi import FastAPI

app = FastAPI()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


async def log_cache_key(cache, key):
    value = await cache.get(key)
    logger.debug(f"cached value {value}")


@app.get("/")
@cached(ttl=1)
async def home() -> Dict[str, int]:
    my_randint: int = random.randint(1, 10000)
    return {"hello": my_randint}


@pytest.fixture
def memory_cache(event_loop):
    cache = SimpleMemoryCache()
    yield cache
    event_loop.run_until_complete(cache.delete("539_cachehome()[]"))
    event_loop.run_until_complete(cache.close())


@pytest.mark.asyncio
async def test_cache(memory_cache):
    async with TestClient(app) as client:
        response = await client.get("/")
        assert response.status_code == 200
        cached_int = response.json().get("hello")
        logger.debug(cached_int)

    async with TestClient(app) as client:
        response = await client.get("/")
        assert response.status_code == 200
        assert response.json().get("hello") == cached_int
        logger.debug(response.json().get("hello"))


@pytest.mark.asyncio
async def test_cache_wait_for_expire(memory_cache):

    async with TestClient(app) as client:
        response = await client.get("/")
        assert response.status_code == 200
        cached_int = response.json().get("hello")
        logger.debug(cached_int)

    await log_cache_key(aiocache.caches.get("default"), "539_cachehome()[]")
    await asyncio.sleep(2)
    await log_cache_key(aiocache.caches.get("default"), "539_cachehome()[]")

    async with TestClient(app) as client:
        response = await client.get("/")
        assert response.status_code == 200
        assert response.json().get("hello") != cached_int
        logger.debug(response.json().get("hello"))
