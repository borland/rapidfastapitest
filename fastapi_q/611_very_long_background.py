import asyncio
import logging
from typing import Dict

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.background import BackgroundTasks

# logging.basicConfig(format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
logger: logging.Logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

if not logger.hasHandlers():
    sh = logging.StreamHandler()
    fmt = logging.Formatter(fmt="%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
    sh.setFormatter(fmt)
    logger.addHandler(sh)
    logger.propagate = False


app = FastAPI()


class Item(BaseModel):
    amount: int


async def background_async(amount: int) -> None:
    logger.debug(f"sleeping {amount}s")
    await asyncio.sleep(amount)
    logger.debug(f"slept {amount}s")


async def background_async_subprocess(amount: int) -> None:
    logger.debug(f"subprocesss in")
    cmd = f"timeout {amount}s tail -f ~/.xsession-errors"
    proc = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )

    stdout, stderr = await proc.communicate()

    logger.debug(len(stdout))
    logger.debug(f"subprocess out with {len(stdout)} bytes")


@app.post("/backgroundasync")
async def sleepingtheback(
    item: Item, background_tasks: BackgroundTasks
) -> Dict[str, str]:
    background_tasks.add_task(background_async, item.amount)
    return {"message": f"sleeping {item.amount} in the back"}


@app.get("/backgroundsubprocess")
async def backgroundsubprocess_list(
    background_tasks: BackgroundTasks
) -> Dict[str, str]:
    background_tasks.add_task(background_async_subprocess, 1200)
    return {"message": f"subprocess in the back"}


if __name__ == "__main__":
    uvicorn.run("611_very_long_background:app", reload=True)
